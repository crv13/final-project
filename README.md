<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Kelompok 11

Anggota Kelompok

- Mochammad Sukamto (@Mochammad.sukamto)
- Arvian Furqon Yudanar (@arvianfy)
- Muhammad Nizar Fazari (@Nizarfazari)

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Tema / Topik Yang Diambil

Website Review Anime. [Theme Wagon Anime](https://themewagon.com/themes/free-bootstrap-4-html5-gaming-anime-website-template-anime/)

## Link Video Demo Web

[Video Demo Web Final Project](https://drive.google.com/file/d/1o2hfMe-lggD29VXS2DXFpAI8E0g66qAD/view?usp=sharing).
