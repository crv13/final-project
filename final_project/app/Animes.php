<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Animes extends Model
{
    protected $table = "animes";
    protected $fillable = ["judul", "ringkasan", "tahun", "poster", "genre_id"];

    public function genres()
    {
        return $this->belongsTo('App\Genres', 'genre_id');
    }
}
