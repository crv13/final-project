<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = "comments";
    protected $fillable = ['point', 'isi','anime_id','user_id'];

    public function user ()
    {
        return $this->belongsToMany('App\User');
    }
    public function anime ()
    {
        return $this->belongsToMany('App\Animes');
    }
}
