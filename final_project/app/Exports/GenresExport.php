<?php

namespace App\Exports;

use App\Genres;
use Maatwebsite\Excel\Concerns\FromCollection;

class GenresExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Genres::all();
    }
}
