<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Genres extends Model
{
    protected $table = "genres";
    protected $fillable = ["nama"];

    public function animes() 
    {
        return $this->hasMany('App\Animes');
    }
}
