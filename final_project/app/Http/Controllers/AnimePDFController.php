<?php

namespace App\Http\Controllers;

use App\Animes;
use Illuminate\Http\Request;
use PDF;

class AnimePDFController extends Controller
{
    public function pdf ($id)
    {
        $animes = Animes::where('id', $id)->first();
        $pdf = PDF::loadView('pdf.animes', compact('animes'));
        return $pdf->download('animes.pdf');
    }
}
