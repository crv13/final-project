<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use App\Animes;
use App\Genres;
use App\Comment;
use File;

class AnimesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $animes = Animes::all();
        return view('animes.index', compact('animes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $genres = Genres::all();
        return view('animes.create', compact('genres'));
        Alert::error('Error', 'Silahkan Coba Kembali');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
    		'judul' => 'required|unique:animes',
    		'ringkasan' => 'required',
            'tahun' => 'required',
            'genre_id' => 'required',
            'poster' => 'required|mimes:jpeg,png,jpg'
    	]);
            
        $gambar = $request->poster;
        $new_gambar = time(). ' - ' . $gambar->getClientOriginalName();

        Animes::create([
    		'judul' => $request->judul,
    		'ringkasan' => $request->ringkasan,
            'tahun' => $request->tahun,
            'genre_id' => $request->genre_id,
            'poster' => $new_gambar
            
    	]);

        $gambar->move('img', $new_gambar);
        
        Alert::success('Berhasil', 'Berhasil Menambahkan Anime Baru');

    	return redirect('/animes');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $comments = Comment::where('anime_id', $id)->get();
        $comment = Comment::where('anime_id', $id)->first();
        $genres = Genres::all();
        $animes = Animes::findorfail($id);

        Alert::info('Info', 'Give Good Comment On Below');

        return view('animes.show', compact('animes', 'genres','comments','comment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $genres = Genres::all();
        $animes = Animes::findorfail($id);
        return view('animes.edit', compact('animes', 'genres'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
    		'judul' => 'required',
    		'ringkasan' => 'required',
            'tahun' => 'required',
            'genre_id' => 'required',
            'poster' => 'mimes:jpeg,png,jpg'
    	]);

        $animes = Animes::findorfail($id);

        if ($request->has('poster')) {
            $path = "img";
            File::delete($path . $animes->poster);
            $gambar = $request->poster;
            $new_gambar = time() . ' - ' . $gambar->getClientOriginalName();
            $gambar->move('img'. $new_gambar);
            $animes_data = [
                'judul' => $request->judul,
                'ringkasan' => $request->ringkasan,
                'tahun' => $request->tahun,
                'genre_id' => $request->genre_id,
                'poster' => $new_gambar
            ];
        } else {
            $animes_data = [
                'judul' => $request->judul,
                'ringkasan' => $request->ringkasan,
                'tahun' => $request->tahun,
                'genre_id' => $request->genre_id
            ];
        }

        $animes->update($animes_data);


        Alert::success('Berhasil', 'Berhasil Diupdate');
        return redirect('/animes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $animes = Animes::findorfail($id);
        $animes->delete();

        $path = "img/";
        File::delete($path . $animes->poster);
        
        Alert::success('Berhasil', 'Berhasil Dihapus');
        return redirect('/animes');
        
    }
}
