<?php

namespace App\Http\Controllers;
use App\Exports\GenresExport;
use App\User;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
Use App\Genres;

class GenresExportController extends Controller
{
    public function export() 
    {
        return Excel::download(new GenresExport, 'Genres.xlsx');
    }
}
