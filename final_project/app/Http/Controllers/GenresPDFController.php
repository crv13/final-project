<?php

namespace App\Http\Controllers;
use PDF;
Use App\Genres;
use Illuminate\Http\Request;

class GenresPDFController extends Controller
{
    public function pdf ()
    {
        $genres = Genres::all();
        $pdf = PDF::loadView('pdf.genres', compact('genres'));
        return $pdf->download('genres.pdf');
    }
    
}
