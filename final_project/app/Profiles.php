<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profiles extends Model
{
    protected $table = 'profiles';
    protected $fillable = ['full_name', 'umur', 'bio', 'user_id'];

    public function user() 
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
