@extends('layouts.master')

@push('script-head')
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
@endpush

@section('content')
@include('layouts.navbar')
    <div class="container">
        <div class="row">
            <div class="col-lg-8 m-auto">
                <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Create New Anime</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form role="form" action="/animes" method="POST" enctype="multipart/form-data">
                    @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="judul">Judul</label>
                        <input type="text" class="form-control" id="judul" name="judul" placeholder="Masukkan Judul Anime">
                        <!-- error handling -->
                        @error('judul')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <!-- penutup error handling -->
                    </div>
                    <div class="form-group">
                        <label for="ringkasan">Ringkasan</label>
                        <textarea name="ringkasan" class="form-control my-editor">{!! old('ringkasan', $ringkasan ?? '') !!}</textarea>
                        <!-- error handling -->
                        @error('ringkasan')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <!-- penutup error handling -->
                    </div>
                    <div class="form-group">
                        <label for="tahun">Tahun</label>
                        <input type="number" class="form-control" id="tahun" name="tahun" placeholder="Masukkan Tahun">
                        <!-- error handling -->
                        @error('tahun')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <!-- penutup error handling -->
                    </div>
                    <div class="form-group">
                        <label for="genre_id">Genre</label><br>
                        <select name="genre_id" id="genre_id" class="form-control">
                            <option value="">-</option>
                            @foreach ($genres as $value)
                                <option value="{{$value->id}}">{{$value->nama}}</option>
                            @endforeach
                        </select>
                        @error('genre_id')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <br><br>
                    <div class="form-group">
                        <label for="poster">Poster</label>
                        <input type="file" class="form-control" id="poster" name="poster" placeholder="Masukkan poster">
                        <!-- error handling -->
                        @error('poster')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                        <!-- penutup error handling -->
                    </div>
                    </div>
                <!-- /.card-body -->
        
                    <div class="card-footer">
                    <button type="submit" class="btn btn-danger">Create</button>
                    </div>
                </form>
                </div>    
            </div>    
        </div>     
    </div>  
</div>
@endsection

@push('script')
<script>
    var editor_config = {
      path_absolute : "/",
      selector: "textarea.my-editor",
      plugins: [
        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime media nonbreaking save table contextmenu directionality",
        "emoticons template paste textcolor colorpicker textpattern"
      ],
      toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
      relative_urls: false,
      file_browser_callback : function(field_name, url, type, win) {
        var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
        var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;
  
        var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
        if (type == 'image') {
          cmsURL = cmsURL + "&type=Images";
        } else {
          cmsURL = cmsURL + "&type=Files";
        }
  
        tinyMCE.activeEditor.windowManager.open({
          file : cmsURL,
          title : 'Filemanager',
          width : x * 0.8,
          height : y * 0.8,
          resizable : "yes",
          close_previous : "no"
        });
      }
    };
  
    tinymce.init(editor_config);
  </script>
@endpush