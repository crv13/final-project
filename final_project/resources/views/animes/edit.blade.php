@extends('layouts.master')

@section('content')
@include('layouts.navbar')
<div class="container">
    <div class="row">
        <div class="col-lg-8 m-auto">
            <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Edit Animes {{$animes->id}}</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" action="/animes/{{$animes->id}}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('put')
                <div class="card-body">
                    <div class="form-group">
                        <label for="judul">Judul</label>
                        <input type="text" class="form-control" name="judul" value="{{$animes->judul}}" id="judul" placeholder="Masukkan Judul">
                        @error('judul')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="ringkasan">Ringkasan</label>
                        <input type="text" class="form-control" name="ringkasan" value="{{$animes->ringkasan}}" id="ringkasan" placeholder="Masukkan Ringkasan">
                        @error('ringkasan')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="tahun">Tahun</label>
                        <input type="number" class="form-control" name="tahun" value="{{$animes->tahun}}" id="tahun" placeholder="Masukkan Tahun">
                        @error('tahun')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="genre_id">Genre</label>
                        <select name="genre_id" id="genre" class="form-control">
                            <option value="">-</option>
                            @foreach ($genres as $value)
                                @if ($value->id == $animes->genre_id)
                                    <option value="{{$value->id}}" selected>{{$value->nama}}</option> 
                                @else
                                    <option value="{{$value->id}}">{{$value->nama}}</option>
                                @endif
                            @endforeach
                        </select>
                        @error('genre_id')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="poster">Poster</label>
                        <input type="file" class="form-control" name="poster" id="poster" placeholder="Masukkan Poster">
                        @error('poster')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary">Update</button>
            </form>
            </div>    
        </div>
    </div>
</div>
</div>
   
@endsection