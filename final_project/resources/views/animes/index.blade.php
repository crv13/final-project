
@extends('layouts.master')


@section('content')
@include('layouts.navbar')
@include('layouts.header')
<div class="container">
    <a href="/animes/create" class="btn btn-primary btn-sm bg-success my-3">Create New Anime</a>
    <div class="row">
        @foreach ($animes as $value)
        <div class="col-lg-3">
            <div class="jumbotron bg-danger">
                <img class="m-auto" width="300px" height="300px" src="{{ asset('img/'.$value->poster)}}" alt="">
                 <hr>
                    <h4 class="text-light">{{$value->judul}} ({{$value->tahun}})</h4>
                    <a href="/genres" class="btn btn-sm btn-info">{{$value->genres->nama}}</a>
                    <p class="text-light">{{$value->ringkasan}}</p>
                    <div class="m-auto">
                        <form action="/animes/{{$value->id}}" method="POST">
                            <a href="/animes/{{$value->id}}" class="btn btn-primary btn-sm">Show</a> 
                            <a href="/animes/{{$value->id}}/edit" class="btn btn-success btn-sm">Edit</a>
                            @csrf
                            @method('DELETE')
                            <input type="submit" value="Delete" class="btn btn-warning btn-sm">
                            <a href="/pdfanimes/{{ $value->id }}" class="btn btn-light btn-sm mt-3">Click to download PDF</a>
                        </form>
                    </div>
                    </div>
                </div>                           
        @endforeach
        </div> 
    </div>
</div>


@endsection
