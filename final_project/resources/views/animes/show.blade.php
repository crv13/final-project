
@extends('layouts.master')

@push('script-head')
<script src="/path-to-your-tinymce/tinymce.min.js"></script>
@endpush

@section('content')
@include('layouts.navbar')
<div class="row">
    <div class="col-lg-8 m-auto">
        <div class="jumbotron">
            <img class="m-auto" width="300px" height="400px" src="{{ asset('img/'.$animes->poster)}}" alt="">
            <hr>
            <a href="#" class="btn btn-warning">Follow </a>
            <a href="#" class="btn btn-warning">Watch</a>
            <hr>
            <h4>{{$animes->judul}} ( {{$animes->tahun}} )</h4>
            <p class="text-info">{{$animes->genres->nama}}</p>
            <p class="text">{{$animes->ringkasan}}</p>
        </div>
        <hr>
        @if ($comment != null)
            @forelse ($comments as $value)   
                <div class="card">
                    <div class="card-body">
                    <h5 class="card-title">ID User : {{$value->user_id}}</h5>
                    <p class="card-text">Komentar : {!! $value->isi !!}</p>
                    <p class="card-text">Point : {{$value->point}}</p>
                    </div>
                </div>
            @empty
               <h1>Belum ada Komentar cuy</h1>   
            @endforelse 
        @else
            @forelse ($comments as $value)
            
            <div class="card">
                <div class="card-body">
                <h5 class="card-title">Nama User</h5>
                <p class="card-text">Komentar User</p>
                <p class="card-text">Point</p>
                <a href="#" class="btn btn-primary">Go somewhere</a>
                </div>
            </div>
            <form action="/comments" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" value={{ $animes->id }} name="id" id="">
                <div class="card-body">
                    <div class="form-group">
                        <label for="isi">Komentar</label>
                        {{-- <input type="text" class="form-control" id="isi" name="isi" placeholder="Masukkan Komentar Anda"> --}}
                        <textarea name="isi" class="form-control my-editor">{!! old('isi', $isi ?? '') !!}</textarea>
                        <!-- error handling -->
                        @error('isi')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <!-- penutup error handling -->
                    </div>
                    <div class="form-group">
                        <label for="point">Point</label>
                        <input type="text" class="form-control" id="point" name="point"  placeholder="Masukkan Penilaian Anda">
                        <!-- error handling -->
                        @error('point')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <!-- penutup error handling -->
                    </div>
                </div>
                <!-- /.card-body -->
            
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Update Profile</button>
                </div>
            </form>
            @empty
                <h1 class="ml-3 text-white" >Belum ada komentar</h1>
                <form action="/comments"  method="POST" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" value={{ $animes->id }} name="id" id="">
                    <div class="card-body">
                    <div class="form-group">
                        <label for="isi" class="text-white">Komentar</label>
                        {{-- <input type="text" class="form-control" id="isi" name="isi" placeholder="Masukkan Komentar Anda"> --}}
                        <textarea name="isi" class="form-control my-editor">{!! old('isi', $isi ?? '') !!}</textarea>
                        <!-- error handling -->
                        @error('isi')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <!-- penutup error handling -->
                    </div>
                    <div class="form-group">
                        <label for="point" class="text-white">Point</label>
                        <input type="text" class="form-control" id="point" name="point" placeholder="Masukkan Penilaian Anda">
                        <!-- error handling -->
                        @error('point')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <!-- penutup error handling -->
                    </div>
                    </div>
                <!-- /.card-body -->
                
                    <div class="card-footer">
                    <button type="submit" class="btn btn-success">Tambah Komentar</button>
                    </div>
                </form>
            @endforelse
            
        @endif                  
    </div>
</div>   
@endsection

@push('script')
<script>
    var editor_config = {
      path_absolute : "/",
      selector: 'textarea.my-editor',
      relative_urls: false,
      plugins: [
        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime media nonbreaking save table directionality",
        "emoticons template paste textpattern"
      ],
      toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
      file_picker_callback : function(callback, value, meta) {
        var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
        var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;
  
        var cmsURL = editor_config.path_absolute + 'laravel-filemanager?editor=' + meta.fieldname;
        if (meta.filetype == 'image') {
          cmsURL = cmsURL + "&type=Images";
        } else {
          cmsURL = cmsURL + "&type=Files";
        }
  
        tinyMCE.activeEditor.windowManager.openUrl({
          url : cmsURL,
          title : 'Filemanager',
          width : x * 0.8,
          height : y * 0.8,
          resizable : "yes",
          close_previous : "no",
          onMessage: (api, message) => {
            callback(message.content);
          }
        });
      }
    };
  
    tinymce.init(editor_config);
  </script>
@endpush