
@extends('layouts.master')

@section('content')
@include('layouts.navbar')
<div class="container">
    <div class="row">
        <div class="col-lg-8 m-auto">
            <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Create New Genre</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" action="{{ route('genres.index') }}" method="POST">
                @csrf
                <div class="card-body">
                <div class="form-group">
                    <label for="nama">Nama Genre</label>
                    <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan Nama Genre">
                    @error('nama')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Create</button>
                    </div>
            </form>
            </div>    
        </div>
    </div>
</div>
        
@endsection