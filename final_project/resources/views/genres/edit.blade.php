@extends('layouts.master')

@section('content')
@include('layouts.navbar')
<div class="container">
    <div class="row">
        <div class="col-lg-8 m-auto">
            <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Edit Genre Anime {{$genres->id}}</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" action="/genres/{{$genres->id}}" method="POST">
                @csrf
                @method('PUT')
                <div class="card-body">
                <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" id="nama" name="nama" value="{{ old('nama', $genres->nama) }}" placeholder="Masukkan Nama genres">
                    <!-- error handling -->
                    @error('nama')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <!-- penutup error handling -->
                </div>
                </div>
            <!-- /.card-body -->
    
                <div class="card-footer">
                <button type="submit" class="btn btn-primary">Update Genre</button>
                </div>
            </form>
            </div>    
        </div>
    </div>
</div>
    
@endsection