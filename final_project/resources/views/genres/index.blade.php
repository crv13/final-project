@extends('layouts.master')

@section('content')
@include('layouts.navbar')
<div class="container">
    <div class="row">
        <div class="col-lg-8 m-auto">
            <div class="card">
                <div class="card-header">
                  <h3 class="card-title">Genre Table</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <!-- Session Succes -->
                    @if(session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
                    <!-- Penutup Session Succes -->
                  <a href="/genres/create" class="btn btn-primary mb-3">Create New Genre</a>
                  <a href="/excell" class="btn btn-success mb-3">Excell </a>
                  <a href="/pdf" class="btn btn-danger mb-3">PDF </a>
                  <table class="table table-bordered">
                    <thead>                  
                      <tr>
                        <th style="width: 10px">#</th>
                        <th>Nama Genre</th>
                        <th style="width: 40px">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                        <!--looping pemanggilan table pada database-->
                        @forelse ($genres as $key => $value) 
                        <tr>
                            <td> {{ $key + 1 }} </td>
                            <td> {{ $value->nama }} </td>
                            <td style="display:flex">
                                <a href="{{ route('genres.edit', ['genre' => $value->id]) }}" class="btn btn-success btn-sm">Edit</a>
                                <form action="{{ route('genres.destroy', ['genre' => $value->id]) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" value="delete" class="btn btn-danger btn-sm">
                                </form>
                            </td>
                        </tr>
                        @empty
                            <tr>
                                <td colspan="4" align="center">No Data</td>
                            </tr>                      
                        @endforelse 
                        <!--penutup looping-->
                    </tbody>
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
        </div>
    </div>
</div>
    
@endsection