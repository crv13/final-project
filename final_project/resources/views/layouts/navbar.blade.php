<!-- Header Section Begin -->
<header class="header">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
                <div class="header__logo">
                    <a href="/animes">
                        <img src="img/logo.png" alt="">
                    </a>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="header__nav">
                    <nav class="header__menu mobile-menu">
                        <ul>
                            <li class="active"><a href="/animes">Homepage</a></li>
                            <li><a href="/genres">Genre</a></li>
                            <li><a href="https://sanbercode.com">Sanbercode</a></li>
                            <li><a href="#">Contacts</a></li>
                            <li><a href="/profiles" class="d-block bg-primary"> {{Auth::user()->username}} </a></li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="header__right">
                          <i class="nav-icon fa fa-sign-out-alt bg-danger">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                            <i class="nav-icon fas fa-sign-out-alt"></i>{{ __('Logout') }}</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">  
                        </i>
                        @csrf
                    </form>
                </div>
            </div>
        </div>
        <div id="mobile-menu-wrap"></div>
    </div>
</header>
<!-- Header End -->