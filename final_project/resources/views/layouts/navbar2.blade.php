<!-- Header Section Begin -->
<header class="header">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
                <div class="header__logo">
                    <a href="./index.html">
                        <img src="img/logo.png" alt="">
                    </a>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="header__nav">
                    <nav class="header__menu mobile-menu">
                        <ul>
                            <li class="active"><a href="/animes">Homepage</a></li>
                            <li><a href="/genres">Genre</a></li>
                            <li><a href="https://sanbercode.com">Sanbercode</a></li>
                            <li><a href="#">Contacts</a></li>
                            <li><a href="/profiles" class="d-block bg-primary">Username</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        <div id="mobile-menu-wrap"></div>
    </div>
</header>
<!-- Header End -->