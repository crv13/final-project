@extends('layouts.master')

@section('content')
@include('layouts.navbar')
        <div class="ml-3 mt-3">
            <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Create New Profile</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" action="/profiles/index" method="POST">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="full_name">Fullname</label>
                        <input type="text" class="form-control" name="full_name" id="full_name" placeholder="Masukkan Nama Lengkap">
                        @error('full_name')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                <div class="card-body">
                <div class="form-group">
                    <label for="umur">Umur</label>
                    <input type="text" class="form-control" name="umur" id="umur" placeholder="Masukkan Umur">
                    @error('nama')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="bio">Biodata</label>
                    <input type="text" class="form-control" name="bio" id="bio" placeholder="Masukkan Nama Genre">
                    @error('nama')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Add Profile</button>
                    </div>
            </form>
            </div>    
        </div>
@endsection