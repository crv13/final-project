@extends('layouts.master')

@section('content')
@include('layouts.navbar')
        @if ($profiles == !null)
        <div class="container">
            <div class="row">
                <div class="col-lg-8 m-auto">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Profile User {{Auth::user()->username}}</h3>
                    </div>
                    <div class="mt-3 mx-4">
                        <p>Nama : {{$profiles->user->name}}</p>
                        <p>Username : {{$profiles->user->username}}</p>
                        <p>Email : {{$profiles->user->email}}</p>
                    </div>
            <!-- /.card-header -->
            <!-- form start -->
                    <form role="form" action="/profiles/{{$profiles->id}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="card-body">
                            <div class="form-group">
                                <label for="full_name">Fullname</label>
                                <input type="text" class="form-control" id="full_name" name="full_name" value="{{ old('full_name', $profiles->full_name) }}" placeholder="Masukkan Umur">
                                <!-- error handling -->
                                @error('full_name')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                <!-- penutup error handling -->
                            </div>
                            <div class="form-group">
                                <label for="umur">Umur</label>
                                <input type="text" class="form-control" id="umur" name="umur" value="{{ old('umur', $profiles->umur) }}" placeholder="Masukkan Umur">
                                <!-- error handling -->
                                @error('umur')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                <!-- penutup error handling -->
                            </div>
                            <div class="form-group">
                                <label for="bio">Biodata</label>
                                <input type="text" class="form-control" id="bio" name="bio" value="{{ old('bio', $profiles->bio) }}" placeholder="Masukkan Biodata">
                                <!-- error handling -->
                                @error('bio')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                <!-- penutup error handling -->
                            </div>
                        </div>
                    <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Update Profile</button>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
        
        @else
        <div class="container">
            <div class="row">
                <div class="col-lg-8 m-auto">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Profile User</h3>
                        </div>
                        <form role="form" action="{{ route('profiles.index') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="full_name">Fullname</label>
                                    <input type="text" class="form-control" name="full_name" id="full_name" placeholder="Masukkan Nama Lengkap">
                                    @error('full_name')
                                        <div class="alert alert-danger">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="umur">Umur</label>
                                    <input type="text" class="form-control" name="umur" id="umur" placeholder="Masukkan Umur">
                                    @error('nama')
                                        <div class="alert alert-danger">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="bio">Biodata</label>
                                    <input type="text" class="form-control" name="bio" id="bio" placeholder="Masukkan Nama Genre">
                                    @error('nama')
                                        <div class="alert alert-danger">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Add Profile</button>
                            </div>
                        </form>
                    </div>
                </div>    
            </div>
        </div>
        @endif  
        

@endsection