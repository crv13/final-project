@include('layouts.navbar')
@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-8 ml-3 mt-3 m-auto">
                <div class="card card-primary bg-danger text-white">
                <div class="card-header">
                    <h3 class="card-title">Create New Cast</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form role="form" action="/user" method="POST">
                    @csrf
                    <div class="card-body">
                    <div class="form-group">
                        <label for="nama">Nama</label>
                        <input type="text" class="form-control" id="nama" name="nama" placeholder="Masukkan Nama User">
                        <!-- error handling -->
                        @error('nama')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <!-- penutup error handling -->
                    </div>
                    <div class="form-group">
                        <label for="username">Username</label>
                        <input type="text" class="form-control" id="username" name="username" placeholder="Masukkan Username">
                        <!-- error handling -->
                        @error('username')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <!-- penutup error handling -->
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="text" class="form-control" id="email" name="email" placeholder="Masukkan Email">
                        <!-- error handling -->
                        @error('email')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <!-- penutup error handling -->
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" id="password" name="password" placeholder="Masukkan Password">
                        <!-- error handling -->
                        @error('password')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <!-- penutup error handling -->
                    </div>
                    </div>
                <!-- /.card-body -->
        
                    <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Create</button>
                    </div>
                </form>
                </div>    
            </div>    
        </div>       
    </div>
@endsection