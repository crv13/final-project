<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(['middleware' => ['auth']], function () {
    Route::resource('animes', 'AnimesController');
    Route::resource('users', 'UserController');
    Route::resource('genres', 'GenresController');
    Route::resource('profiles', 'ProfilesController')->only('index', 'store', 'update');
    Route::resource('comments', 'CommentController')->only(['store','update']);
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('excell', 'GenresExportController@export');
    Route::get('pdf', 'GenresPDFController@pdf');
    Route::get('/pdfanimes/{id}', 'AnimePDFController@pdf');
});



Auth::routes();

Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});
